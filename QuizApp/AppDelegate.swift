//
//  AppDelegate.swift
//  QuizApp
//
//  Created by five on 07/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var navigationController: UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        setRootViewController()
        Quiz.closeAllQuizes()
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func setRootViewController() {
        let viewController: UIViewController
        
        let userDefaults = UserDefaults.standard
        if let value = userDefaults.string(forKey: Utility.tokenString) {
            viewController = TabBarViewController()
        } else {
            viewController = LoginViewController()
        }
        
        window?.rootViewController = viewController
    }

}

