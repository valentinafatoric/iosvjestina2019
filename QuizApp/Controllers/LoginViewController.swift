//
//  LoginViewController.swift
//  QuizApp
//
//  Created by five on 08/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var quizImage: UIImageView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var quizImageView: UIImageView!
    
    @IBOutlet weak var clearButton: UIButton!
    
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var usernameField: UITextField!
    
    @IBAction func clearAll(_ sender: Any) {
        usernameField.text = ""
        passwordField.text = ""
    }
    
    @IBAction func logIn(_ sender: Any) {
        let logInService = LogInService()
        
        guard let usernameText = usernameField.text else { return }
        guard let passwordText = passwordField.text else { return }
        
        logInService.getAutentificationToken(username: usernameText, password: passwordText){ (message) in
            if let message = message {
                Utility.showToast(controller: self, message: message, seconds: 5.0)
            } else {
                DispatchQueue.main.async {
                    self.animateEverythingOut()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.quizImage.transform = self.quizImage.transform.scaledBy(x: 0.01, y: 0.01)
        
        self.usernameLabel.transform = self.usernameLabel.transform.translatedBy(x: -100, y: 0)
        self.usernameField.transform = self.usernameField.transform.translatedBy(x: -100, y: 0)
        
        self.passwordLabel.transform = self.passwordLabel.transform.translatedBy(x: -100, y: 0)
        self.passwordField.transform = self.passwordField.transform.translatedBy(x: -100, y: 0)
        
        self.clearButton.transform = self.clearButton.transform.translatedBy(x: -100, y: 0)
        self.logInButton.transform = self.logInButton.transform.translatedBy(x: -100, y: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupKeyboard()
        animateEverythingIn()
    }
    
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = getKeyboardHeight(notification: notification) {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func getKeyboardHeight(notification: NSNotification) -> CGFloat? {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            return keyboardHeight
        }
        return nil
    }
    
    func animateEverythingIn() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.quizImage.alpha = 1.0
            self.quizImage.transform = self.quizImage.transform.scaledBy(x: 100, y: 100)
         }) { _ in
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.usernameLabel.alpha = 1.0
            self.usernameLabel.transform = self.usernameLabel.transform.translatedBy(x: 100, y: 0)
            
            self.usernameField.alpha = 1.0
            self.usernameField.transform = self.usernameField.transform.translatedBy(x: 100, y: 0)
         }) { _ in
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.6, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.passwordLabel.alpha = 1.0
            self.passwordLabel.transform = self.passwordLabel.transform.translatedBy(x: 100, y: 0)
            
            self.passwordField.alpha = 1.0
            self.passwordField.transform = self.passwordField.transform.translatedBy(x: 100, y: 0)
         }) { _ in
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.9, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.clearButton.alpha = 1.0
            self.clearButton.transform = self.clearButton.transform.translatedBy(x: 100, y: 0)
            
            self.logInButton.alpha = 1.0
            self.logInButton.transform = self.logInButton.transform.translatedBy(x: 100, y: 0)
         }) { _ in
        }
    }
    
    func animateEverythingOut() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.quizImage.transform = self.quizImage.transform.translatedBy(x: 0, y: -500)
         }) { _ in
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.usernameLabel.transform = self.usernameLabel.transform.translatedBy(x: 0, y: -600)
            
            self.usernameField.transform = self.usernameField.transform.translatedBy(x: 0, y: -600)
         }) { _ in
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.6, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.passwordLabel.transform = self.passwordLabel.transform.translatedBy(x: 0, y: -700)
            
            self.passwordField.transform = self.passwordField.transform.translatedBy(x: 0, y: -700)
         }) { _ in
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.9, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.clearButton.transform = self.clearButton.transform.translatedBy(x: 0, y: -800)
            
            self.logInButton.transform = self.logInButton.transform.translatedBy(x: 0, y: -800)
         }) { _ in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.setRootViewController()
        }
    }
    
}
