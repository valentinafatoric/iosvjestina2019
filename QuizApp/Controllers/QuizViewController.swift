//
//  QuizViewController.swift
//  QuizApp
//
//  Created by five on 09/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit
import Kingfisher

class QuizViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
   
    var viewModel: QuizViewModel!
    
    var startTime: Date = Date()
    
    var numberOfCorrectAnswers: Int = 0
    
    weak var delegate: QuestionViewDelegate?
    
    convenience init(viewModel: QuizViewModel) {
        self.init()
        self.viewModel = viewModel
    }
    
    deinit {
        if !scrollView.isHidden {
            Quiz.setOpened(id: viewModel.id, opened: false)
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    func bindViewModel() {
        titleLabel.text = viewModel.title
        imageView.kf.setImage(with: viewModel.imageUrl)
        self.view.backgroundColor = viewModel.category.color
        
        setUpScrollView()
    }
    
    func setUpScrollView() {
        let questions = viewModel.questions
        
        scrollView.isHidden = true
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(questions.count), height: scrollView.frameLayoutGuide.layoutFrame.height)
        
        for i in 0 ..< questions.count {
            let subview: QuestionView = QuestionView()
            subview.set(question: questions[i])
            subview.frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: scrollView.frameLayoutGuide.layoutFrame.width, height: scrollView.frameLayoutGuide.layoutFrame.height)
            subview.delegate = self
            subview.backgroundColor = viewModel.category.color.withAlphaComponent(0.3)
            scrollView.addSubview(subview)
        }
        
        pageControl.numberOfPages = questions.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    @IBAction func startQuiz(_ sender: Any) {
        let quiz = DataController.shared.getQuiz(id: viewModel.id)
        
        if let opened = quiz?.isOpened() {
            if(opened) {
                DispatchQueue.main.async {
                    self.showAlertForOpenedQuiz()
                }
            } else {
                Quiz.setOpened(id: viewModel.id, opened: true)
                
                scrollView.isHidden = false
                startTime = Date()
                numberOfCorrectAnswers = 0
            }
        }
    }
    
    func showAlertForOpenedQuiz() {
        let alert = UIAlertController(title: "Quiz opened!", message: "Quiz is already opened! Finish it and then start again!", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func seeResults(_ sender: Any) {
        getResults()
    }
    
    func getResults() {
        ResultsService().getResults(quizId: Int(truncating: viewModel.quiz?.id ?? 0)) { (results) in
            if let results = results {
                let resultsViewModel = ResultsViewModel()
                resultsViewModel.quiz = self.viewModel.quiz
                resultsViewModel.results = results
                DispatchQueue.main.async {
                    let resultsViewController = ResultsViewController(viewModel: resultsViewModel)
                
                    self.navigationController?.pushViewController(resultsViewController, animated: true)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlertForNoResults()
                }
            }
        }
    }
    
    func showAlertForNoResults() {
        let alert = UIAlertController(title: "No results", message: "There aren't any results for this quiz!", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
        }))
        alert.addAction(UIAlertAction(title: "Try again",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.getResults()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension QuizViewController: QuestionViewDelegate {
    
    func answerClicked(isCorrectAnswer: Bool) {
        if (isCorrectAnswer) {
            numberOfCorrectAnswers += 1
        }
        
        if(pageControl.currentPage < viewModel.questions.count - 1) {
            pageControl.currentPage += 1
            scrollToPage(page: pageControl.currentPage, animated: true)
        } else {
            self.sendResult()
        }
    }
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.scrollView.frameLayoutGuide.layoutFrame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: animated)
    }
    
    func sendResult() {
        let time = Double(Date().timeIntervalSince(startTime))
        ResultsService().sendResults(quizId: viewModel.id, time: time, numberOfCorrect: numberOfCorrectAnswers){ (responseCode) in
            Quiz.setOpened(id: self.viewModel.id, opened: false)
            
            if responseCode == ResponseCodes.ok {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController( animated: true)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(responseCode: responseCode)
                }
            }
        }
    }
    
    func showAlert(responseCode: ResponseCodes?) {
        let defaultErrorMessage = "There wasn't any response!"
        
        let alert = UIAlertController(title: "Send again?", message: "Error was: \(responseCode?.message ?? defaultErrorMessage)", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            self.navigationController?.popViewController( animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Send again",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.sendResult()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
