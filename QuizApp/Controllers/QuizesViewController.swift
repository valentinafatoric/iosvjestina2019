//
//  QuizesViewController.swift
//  QuizApp
//
//  Created by five on 21/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

class QuizesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
        
    var refreshControl: UIRefreshControl!
    
    let cellReuseIdentifier = "cellReuseIdentifier"
    
    var viewModel: QuizesViewModel!
    
    convenience init(viewModel: QuizesViewModel) {
        self.init()
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        bindViewModel()
    }
    
    func setupTableView() {
        tableView.backgroundColor = UIColor.lightGray
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(QuizesViewController.refresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl

        tableView.register(UINib(nibName: "QuizesTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func bindViewModel() {
        viewModel.fetchQuizes {
            self.refresh()
        }
    }
    
    @objc func refresh() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
}

extension QuizesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = QuizesTableSectionHeader()
        let category = QuizCategory.init(rawValue: viewModel.quizCategories[section])
        view.titleLabel.text = category?.rawValue
        view.titleLabel.backgroundColor = category?.color
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let viewModel = viewModel.viewModel(atIndex: indexPath.row, section: indexPath.section) {
            let quizViewController = QuizViewController(viewModel: viewModel)
            navigationController?.pushViewController(quizViewController, animated: true)
        }
    }
}

extension QuizesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! QuizesTableViewCell
        
        if let quiz = viewModel.quiz(atIndex: indexPath.row, section: indexPath.section) {
            cell.setup(withQuiz: quiz)
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfCategories()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfQuizes(section: section)
    }
}
