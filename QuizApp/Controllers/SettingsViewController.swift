//
//  SettingsViewController.swift
//  QuizApp
//
//  Created by five on 12/06/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBAction func logOut(_ sender: UIButton) {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: Utility.tokenString)
        userDefaults.removeObject(forKey: Utility.userIdString)
        userDefaults.removeObject(forKey: Utility.usernameString)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setRootViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaults.standard
        guard let username = userDefaults.string(forKey: Utility.usernameString) else { return }
        
        usernameLabel.text = "Username: " + username
    }
}
