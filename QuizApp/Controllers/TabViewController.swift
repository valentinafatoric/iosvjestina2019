//
//  TabViewController.swift
//  QuizApp
//
//  Created by five on 12/06/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let vc = QuizesViewController(viewModel: QuizesViewModel())
        let nvc = UINavigationController(rootViewController: vc)
        
        vc.navigationItem.title = "Quizes"
        nvc.tabBarItem = UITabBarItem(title: "Quizes", image: nil, tag: 0)
        
        let vc2 = SearchViewController(viewModel: QuizesViewModel())
        let nvc2 = UINavigationController(rootViewController: vc2)
        vc2.navigationItem.title = "Search"
        nvc2.tabBarItem = UITabBarItem(title: "Search", image: nil, tag: 0)
        
        let vc3 = SettingsViewController()
        vc3.navigationItem.title = "Settings"
        vc3.tabBarItem = UITabBarItem(title: "Settings", image: nil, tag: 0)
        
        self.viewControllers = [nvc, nvc2, vc3]
    }
}
