//
//  Question+CoreDataClass.swift
//  QuizApp
//
//  Created by five on 13/06/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation
import CoreData
import UIKit

@objc(Question)
public class Question: NSManagedObject {
    
    class func getEntityName() -> String {
        return "Question"
    }
    
    class func firstOrCreate(withId id: Int) -> Question? {
        let context = DataController.shared.persistentContainer.viewContext
        
        let request: NSFetchRequest<Question> = Question.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", NSNumber(value: id))
        request.returnsObjectsAsFaults = false

        do {
            let questions = try context.fetch(request)
            if let question = questions.first {
                return question
            } else {
                let newQuestion = Question(context: context)
                return newQuestion
            }
        } catch {
            return nil
        }
    }
    
    class func createFrom(json: [String: Any]) -> Question? {
        if let id = json["id"] as? Int,
            let questionString = json["question"] as? String,
            let answers = json["answers"] as? [String],
            let correctAnswer = json["correct_answer"] as? Int {
            
            if let question = Question.firstOrCreate(withId: id) {
                question.id = NSNumber(value: id)
                question.question = questionString
                
                question.answers = answers.description
                question.correctAnswer = NSNumber(value: correctAnswer)
                
                DataController.shared.saveContext()
                return question
            }
        } else {
            return nil
        }
        return nil
    }
}
