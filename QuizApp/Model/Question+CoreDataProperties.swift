//
//  Question+CoreDataProperties.swift
//  QuizApp
//
//  Created by five on 13/06/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation
import CoreData


extension Question {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Question> {
        return NSFetchRequest<Question>(entityName: "Question")
    }

    @NSManaged public var id: NSNumber?
    @NSManaged public var question: String?
    @NSManaged public var answers: String?
    @NSManaged public var correctAnswer: NSNumber?
    
    func getAnswersAsArray() -> [String] {
        let answersAsData = answers!.data(using: String.Encoding.utf16)
        let answersArray: [String] = try! JSONDecoder().decode([String].self, from: answersAsData!)
        return answersArray
    }
}
