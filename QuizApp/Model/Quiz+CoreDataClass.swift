//
//  Quiz+CoreDataClass.swift
//  QuizApp
//
//  Created by five on 13/06/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation
import CoreData
import UIKit

@objc(Quiz)
public class Quiz: NSManagedObject {
    
    class func getEntityName() -> String {
        return "Quiz"
    }
    
    class func firstOrCreate(withId id: Int) -> Quiz? {
        let context = DataController.shared.persistentContainer.viewContext
        
        let request: NSFetchRequest<Quiz> = Quiz.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", NSNumber(value: id))
        request.returnsObjectsAsFaults = false

        do {
            let quizes = try context.fetch(request)
            if let quiz = quizes.first {
                return quiz
            } else {
                let newQuiz = Quiz(context: context)
                return newQuiz
            }
        } catch {
            return nil
        }
    }
    
    class func closeAllQuizes() {
        let context = DataController.shared.persistentContainer.viewContext
        
        let request: NSFetchRequest<Quiz> = Quiz.fetchRequest()
        request.returnsObjectsAsFaults = false

        do {
            let quizes = try context.fetch(request)
            for quiz in quizes {
                quiz.opened = NSNumber(value: false)
            }
            
            DataController.shared.saveContext()
        } catch {
            print("Error while closing quizes!")
        }
    }
    
    class func setOpened(id: Int, opened: Bool) {
        if let quiz = Quiz.firstOrCreate(withId: id) {
            quiz.opened = NSNumber(value: opened)
            DataController.shared.saveContext()
        }
    }
    
    class func createFrom(json: [String: Any]) -> Quiz? {
        if let id = json["id"] as? Int,
            let title = json["title"] as? String,
            let description = json["description"] as? String,
            let category = json["category"] as? String,
            let level = json["level"] as? Int,
            let imageUrl = json["image"] as? String,
            let questions = json["questions"] as? [[String: Any]] {
            
            if let quiz = Quiz.firstOrCreate(withId: id) {
                quiz.id = NSNumber(value: Int(id))
                quiz.title = title
                quiz.desc = description
                quiz.category = category
                quiz.level = NSNumber(value: level)
                quiz.imageUrl = imageUrl
            
                var questionsSet = Set<Question>()
                
                for questionString in questions {
                    guard let question = Question.createFrom(json: questionString) else { continue }
                    questionsSet.insert(question)
                }
                
                quiz.questions = questionsSet
                
                DataController.shared.saveContext()
                return quiz
            }
        } else {
            return nil
        }
        return nil
    }
}
