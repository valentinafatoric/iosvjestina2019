//
//  Quiz+CoreDataProperties.swift
//  QuizApp
//
//  Created by five on 13/06/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation
import CoreData


extension Quiz {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Quiz> {
        return NSFetchRequest<Quiz>(entityName: "Quiz")
    }

    @NSManaged public var id: NSNumber?
    @NSManaged public var title: String?
    @NSManaged public var desc: String?
    @NSManaged public var category: String?
    @NSManaged public var level: NSNumber?
    @NSManaged public var imageUrl: String?
    @NSManaged public var questions: Set<Question>?
    @NSManaged public var opened: NSNumber?
    
    func isOpened() -> Bool {
        return Bool(truncating: opened ?? 0)
    }

}
