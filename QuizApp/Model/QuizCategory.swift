//
//  QuizCategory.swift
//  QuizApp
//
//  Created by five on 09/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation
import UIKit

enum QuizCategory: String, CaseIterable {
    case sports = "SPORTS"
    case science = "SCIENCE"
    case none = ""
    
    var text: String {
        switch self {
        case .sports:
            return QuizCategory.sports.rawValue
        case .science:
            return QuizCategory.science.rawValue
        case .none:
            return QuizCategory.none.rawValue
        }
    }
    
    var color: UIColor {
        switch self {
        case .sports:
            return UIColor.blue
        case .science:
            return UIColor.yellow
        case .none:
            return UIColor.white
        }
    }
}
