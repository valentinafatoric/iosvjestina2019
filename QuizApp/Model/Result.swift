//
//  Result.swift
//  QuizApp
//
//  Created by five on 30/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation

struct Result: Codable {
    let username: String
    let score: String?
    
    enum CodingKeys: String, CodingKey {
        case username
        case score
    }
}
