//
//  LogInService.swift
//  QuizApp
//
//  Created by five on 08/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation

class LogInService {
    
    var failMessage: String = "Can't log in this user!"
    
    func getAutentificationToken(username: String, password: String, completion: @escaping ((String?) -> Void)) {
        let url = URL(string: Utility.urlString + "/session")
        var request = URLRequest(url: url!)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let parameters: [String: Any] = ["username": username, "password": password]
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    let jsonDict = json as? [String: Any]
                    let token = jsonDict![Utility.tokenString] as? String
                    let userId = jsonDict![Utility.userIdString] as? Int
                    
                    if let token = token, let userId = userId {
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userId, forKey: Utility.userIdString)
                        userDefaults.set(token, forKey: Utility.tokenString)
                        userDefaults.set(username, forKey: Utility.usernameString)
                        completion(nil)
                    } else {
                        completion(self.failMessage)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                completion(self.failMessage)
            }
        }
        
        dataTask.resume()
    }
}
