//
//  QuizesService.swift
//  QuizApp
//
//  Created by five on 09/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation
import Reachability

class QuizesService {
    
    func fetchQuizes(completion: @escaping (([Quiz]?) -> Void)) {
        let reachability = try! Reachability()
        
        if reachability.connection == .unavailable {
            let quizzes = DataController.shared.fetchQuizes()
            completion(quizzes)
        } else {
            let url = URL(string: Utility.urlString + "/quizzes")
            var request = URLRequest(url: url!)
           
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           
            let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
               
               if let data = data {
                   do {
                       let json = try JSONSerialization.jsonObject(with: data, options: [])
                       
                       let jsonDict = json as? [String: Any]
                       let quizzesStringArray = jsonDict!["quizzes"] as? [[String: Any]]
                       
                       var quizzes: [Quiz] = []
                       
                       for quizzesString in quizzesStringArray! {
                           let quiz = Quiz.createFrom(json: quizzesString)
                           if let quiz = quiz {
                               quizzes.append(quiz)
                           }
                       }
                           
                       completion(quizzes)
                   } catch {
                       print(error.localizedDescription)
                   }
               } else {
                   completion(nil)
               }
           }
           
           dataTask.resume()
        }
    }

}
