//
//  ResultsService.swift
//  QuizApp
//
//  Created by five on 30/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation

class ResultsService {
    
    func sendResults(quizId: Int, time: Double, numberOfCorrect: Int, completion: @escaping ((ResponseCodes?) -> Void)) {
        let url = URL(string: Utility.urlString + "/result")
        var request = URLRequest(url: url!)
        
        let userDefaults = UserDefaults.standard
        guard let token = userDefaults.string(forKey: Utility.tokenString) else { return }
        let userId = userDefaults.integer(forKey: Utility.userIdString)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        let parameters: [String: Any] = ["quiz_id": quizId, "user_id": userId, "time": time, "no_of_correct": numberOfCorrect]
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse {
                let responseCode = ResponseCodes.init(rawValue: httpResponse.statusCode)
                completion(responseCode)
            }
            
            completion(nil)
        }
        
        dataTask.resume()
    }
    
    func getResults(quizId: Int, completion: @escaping (([Result]?) -> Void)) {
        let url = URL(string: Utility.urlString + "/score?quiz_id=\(quizId)")
        var request = URLRequest(url: url!)
        
        let userDefaults = UserDefaults.standard
        guard let token = userDefaults.string(forKey: Utility.tokenString) else { return }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    let results = try JSONDecoder().decode([Result].self, from: data)
                    
                    let sortedResults = results.sorted(by: { Double($0.score ?? "0.0") ?? 0 > Double($1.score ?? "0.0") ?? 0 })
                    
                    completion(sortedResults)
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                completion(nil)
            }
        }
        
        dataTask.resume()
    }
    
}

