//
//  Utility.swift
//  QuizApp
//
//  Created by five on 09/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    
    static var tokenString: String = "token"
    
    static var userIdString: String = "user_id"
    
    static var usernameString: String = "username"
    
    static var urlString: String = "https://iosquiz.herokuapp.com/api"
    
    static func showToast(controller: UIViewController, message : String, seconds: Double) {
        DispatchQueue.main.async {
            let alertMessage = UIAlertController(title: "", message: message, preferredStyle: .alert)

            let cancelAction = UIAlertAction(title: "Ok", style: .cancel)
            alertMessage.addAction(cancelAction)
            
            controller.present(alertMessage, animated: true, completion: nil)
        }
    }
}
