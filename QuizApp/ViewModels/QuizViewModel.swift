//
//  QuizViewModel.swift
//  QuizApp
//
//  Created by five on 30/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation

class QuizViewModel {
    var quiz: Quiz? = nil
        
    init(quiz: Quiz) {
        self.quiz = quiz
    }
    
    var id: Int {
        return Int(truncating: quiz?.id ?? 0)
    }

    var title: String {
        return quiz?.title!.uppercased() ?? ""
    }
    
    var description: String {
        return quiz?.description ?? ""
    }
    
    var level: Int {
        return Int(truncating: quiz?.level ?? 0)
    }
    
    var category: QuizCategory {
        return QuizCategory(rawValue: quiz?.category ?? "") ?? QuizCategory.none
    }
    
    var questions: [Question] {
        return Array(quiz?.questions ?? Set<Question>()) 
    }
    
    var imageUrl: URL? {
        if let urlString = quiz?.imageUrl {
            return URL(string: urlString)
        }
        return nil
    }
    
}
