//
//  QuizesViewModel.swift
//  QuizApp
//
//  Created by five on 28/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation

struct QuizesCellModel {
    
    let id: Int
    let title: String
    let description: String
    let category: QuizCategory
    let level: Int
    let imageUrl: String
    var questions: [Question]
    
    init(quiz: Quiz) {
        self.id = quiz.id as! Int
        self.title = quiz.title!
        self.description = quiz.desc!
        self.category = QuizCategory(rawValue: quiz.category ?? "") ?? QuizCategory.none
        self.level = Int(truncating: quiz.level ?? 0)
        self.imageUrl = quiz.imageUrl!
        self.questions = Array(quiz.questions ?? [])
    }
}

class QuizesViewModel {
    
    var quizes: [Quiz]?
    
    var quizCategories: [String] = []
    
    var quizesByCategory: [String: [Quiz]] = [:]
    
    func fetchQuizes(completion: @escaping (() -> Void))  {
        QuizesService().fetchQuizes { [weak self] (quizes) in
            self?.quizes = quizes
            self?.quizesByCategory = Dictionary(grouping: (self?.quizes)!,
                                                by: { quiz in quiz.category! })
            self?.quizCategories = Array((self?.quizesByCategory.keys)!)
            
            completion()
        }
    }
    
    func searchQuizes(searchBy: String, completion: @escaping (() -> Void)) {
        self.quizes = DataController.shared.searchQuizes(searchBy: searchBy)
        self.quizesByCategory = Dictionary(grouping: (self.quizes)!,
                                            by: { quiz in quiz.category! })
        self.quizCategories = Array(self.quizesByCategory.keys)
        
        completion()
    }
    
    func viewModel(atIndex index: Int, section: Int) -> QuizViewModel? {
        guard let quizesForCategory = quizesByCategory[quizCategories[section]] else {
            return nil
        }
        
        if index >= 0 && index < quizesForCategory.count {
            let quizesViewModel = QuizViewModel(quiz: quizesForCategory[index])
            return quizesViewModel
        }
        
        return nil
    }

    func quiz(atIndex index: Int, section: Int) -> QuizesCellModel? {
        guard let quizesForCategory = quizesByCategory[quizCategories[section]] else {
            return nil
        }
        
        let quizesCellModel = QuizesCellModel(quiz: quizesForCategory[index])
        return quizesCellModel
    }
    
    func numberOfQuizes(section: Int) -> Int {
        guard let quizesForCategory = quizesByCategory[quizCategories[section]] else {
            return 0
        }
        
        return quizesForCategory.count
    }
    
    func numberOfCategories() -> Int {
        return quizesByCategory.count
    }
}
