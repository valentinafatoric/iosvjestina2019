//
//  ResultsViewModel.swift
//  QuizApp
//
//  Created by five on 30/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import Foundation

struct ResultsCellModel {
    
    let id: String
    let username: String
    let score: String
    
    init(id: Int, result: Result) {
        self.id = String(id + 1) + "."
        self.username = result.username
        self.score = result.score ?? "No score"
    }
}

class ResultsViewModel {
    
    var quiz: Quiz?
    
    var results: [Result]?
    
    func fetchResults(completion: @escaping (() -> Void))  {
        ResultsService().getResults(quizId: Int(truncating: quiz?.id ?? 0)) { [weak self] (results) in
            self?.results = results
            
            completion()
        }
    }

    func result(atIndex index: Int) -> ResultsCellModel? {
        guard let results = results else {
            return nil
        }
        
        let resultCellModel = ResultsCellModel(id: index, result: results[index])
        return resultCellModel
    }
    
    func numberOfResults() -> Int {
        guard let results = results else {
            return 0
        }
        
        return min(results.count, 20)
    }
}

