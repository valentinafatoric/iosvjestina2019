//
//  QuestionView.swift
//  QuizApp
//
//  Created by five on 09/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

protocol QuestionViewDelegate: class {
    func answerClicked(isCorrectAnswer: Bool)
}

class QuestionView: UIView {

    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var answerZeroButton: UIButton!
    
    @IBOutlet weak var answerOneButton: UIButton!
    
    @IBOutlet weak var answerTwoButton: UIButton!
    
    @IBOutlet weak var answerThreeButton: UIButton!
    
    @IBOutlet var questionView: UIView!
    
    var question: Question?
    
    weak var delegate: QuestionViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createView()
    }
    
    func createView() {
        Bundle.main.loadNibNamed("QuestionView", owner: self, options: nil)
        addSubview(questionView)
        questionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func set(question: Question) {
        self.question = question
        questionLabel.text = question.question
        let answers: [String] = question.getAnswersAsArray()
        
        answerZeroButton.setTitle(answers[0], for: .normal)
        answerOneButton.setTitle(answers[1], for: .normal)
        answerTwoButton.setTitle(answers[2], for: .normal)
        answerThreeButton.setTitle(answers[3], for: .normal)
        
        answerZeroButton.backgroundColor = UIColor.white
        answerOneButton.backgroundColor = UIColor.white
        answerTwoButton.backgroundColor = UIColor.white
        answerThreeButton.backgroundColor = UIColor.white
    }
    
    @IBAction func answerClicked(_ sender: UIButton) {
        let correctAnswer = self.question?.getAnswersAsArray()[Int(truncating: self.question!.correctAnswer ?? 0)]
        let answer = sender.titleLabel!.text!
        
        if answer.isEqual(correctAnswer) {
            sender.backgroundColor = UIColor.green
        } else {
            sender.backgroundColor = UIColor.red
        }
        
        delegate?.answerClicked(isCorrectAnswer: answer.isEqual(correctAnswer))
    }
}
