//
//  QuizesTableSectionHeader.swift
//  QuizApp
//
//  Created by five on 21/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit
import PureLayout

class QuizesTableSectionHeader: UIView {

    var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.lightGray
        titleLabel = UILabel()
        titleLabel.text = ""
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        titleLabel.textColor = UIColor.darkGray
        titleLabel.textAlignment = .center;
        self.addSubview(titleLabel)
        titleLabel.autoPinEdge(.top, to: .top, of: self)
        titleLabel.autoPinEdge(.bottom, to: .bottom, of: self)
        titleLabel.autoPinEdge(.left, to: .left, of: self)
        titleLabel.autoAlignAxis(.vertical, toSameAxisOf: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

