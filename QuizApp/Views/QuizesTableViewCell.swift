//
//  QuizTableViewCell.swift
//  QuizApp
//
//  Created by five on 21/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit
import Kingfisher

class QuizesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var quizImageView: UIImageView!
    
    @IBOutlet weak var quizDescription: UILabel!
    
    @IBOutlet weak var quizTitle: UILabel!
    
    @IBOutlet weak var firstStarImageView: UIImageView!
    
    @IBOutlet weak var secondStarImageView: UIImageView!
   
    @IBOutlet weak var thirdStarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        quizDescription.text = ""
        quizTitle.text = ""
        quizImageView?.image = nil
        firstStarImageView?.image = nil
        secondStarImageView?.image = nil
        thirdStarImageView?.image = nil
    }
    
    func setup(withQuiz quiz: QuizesCellModel) {
        quizDescription.text = quiz.description
        quizTitle.text = quiz.title
        
        let imageUrl = URL(string: quiz.imageUrl)
        quizImageView.kf.setImage(with: imageUrl)
        
        setStarImage(imageView: firstStarImageView, level: quiz.level, imageNumber: 1)
        setStarImage(imageView: secondStarImageView, level: quiz.level, imageNumber: 2)
        setStarImage(imageView: thirdStarImageView, level: quiz.level, imageNumber: 3)
    }
    
    func setStarImage(imageView: UIImageView?, level: Int, imageNumber: Int) {
        if(imageNumber <= level) {
            imageView?.image = UIImage(named: "full-star")
        } else {
            imageView?.image = UIImage(named: "empty-star")
        }
    }
}
