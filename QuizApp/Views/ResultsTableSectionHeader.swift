//
//  ResultsTableSectionHeader.swift
//  QuizApp
//
//  Created by five on 30/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

class ResultsTableSectionHeader: UIView {

    var titleLabel: UILabel!
    
    var usernameLabel: UILabel!
    
    var scoreLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.lightGray
        
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height / 2))
        titleLabel.text = "Results"
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        titleLabel.textColor = UIColor.darkGray
        titleLabel.textAlignment = .center
        self.addSubview(titleLabel)
        
        let labelWidth = (frame.width - 50) / 2
        
        usernameLabel = UILabel(frame: CGRect(x: 50, y: frame.height / 2, width: labelWidth, height: frame.height / 2))
        usernameLabel.text = "Username"
        usernameLabel.font = UIFont.systemFont(ofSize: 14)
        usernameLabel.textColor = UIColor.darkGray
        usernameLabel.textAlignment = .center
        self.addSubview(usernameLabel)
        
        scoreLabel = UILabel(frame: CGRect(x: 50 + labelWidth, y: frame.height / 2, width: labelWidth, height: frame.height / 2))
        scoreLabel.text = "Score"
        scoreLabel.font = UIFont.systemFont(ofSize: 14)
        scoreLabel.textColor = UIColor.darkGray
        scoreLabel.textAlignment = .center
        self.addSubview(scoreLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
