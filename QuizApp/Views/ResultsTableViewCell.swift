//
//  ResultTableViewCell.swift
//  QuizApp
//
//  Created by five on 30/05/2020.
//  Copyright © 2020 five. All rights reserved.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var indexLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        usernameLabel.text = ""
        scoreLabel.text = ""
        indexLabel.text = ""
    }
    
    func setup(withResult result: ResultsCellModel) {
        usernameLabel.text = result.username
        scoreLabel.text = result.score
        indexLabel.text = result.id
    }
}
